import {calculateTimeSeries} from '../utils';
import {test} from "@jest/globals";
import cones from '../../cones';
import testObj from './testObj.json';


const testData = {mu: cones[0].mu,
    sigma: cones[0].sigma,
    years: 10,
    initialSum: 10000,
    monthlySum: 200,
    fee: 0.01}

test('return an Object', () => {
    expect(calculateTimeSeries(testData)).toBeInstanceOf(Object);
});

test('Object contains expected properties', () => {
    expect(calculateTimeSeries(testData)).toHaveProperty('lower05');
    expect(calculateTimeSeries(testData)).toHaveProperty('median');
    expect(calculateTimeSeries(testData)).toHaveProperty('upper95');
});

test('match test Object', () => {
    expect(calculateTimeSeries(testData)).toMatchObject(testObj);
});