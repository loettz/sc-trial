import React from 'react';
import PropTypes from 'prop-types';

class RiskLevelSelector extends React.Component {

    constructor(props) {
        super(props);
        this.onChangeRisk = this.onChangeRisk.bind(this);
        this.onChangeInvestment = this.onChangeInvestment.bind(this);
    }

    onChangeRisk(event) {
        let {onChangeRiskLevel} = this.props;
        let riskLevel = parseInt(event.target.value);
        onChangeRiskLevel(riskLevel);
    }

    onChangeInvestment(event) {
        let {onChangeInvestmentSum} = this.props;
        let investmentSum = parseInt(event.target.value);
        onChangeInvestmentSum(investmentSum);
    }

    render() {
        const {minRiskLevel, maxRiskLevel, riskLevel, investmentSum} = this.props;
        const options = [];
        for(let k=minRiskLevel; k<=maxRiskLevel; ++k) {
            options.push(
                <option key={k} value={k}>{k}</option>
            );
        }

        return (
            <form>
                <label>Risk level:
                    <select onChange={this.onChangeRisk} defaultValue={riskLevel}>
                        {options}
                    </select>
                </label>
                <label>
                    Investment:
                    <input defaultValue={investmentSum} onChange={this.onChangeInvestment} id="investment" min="1" type="number" />
                </label>
            </form>
        );
    }
}

RiskLevelSelector.defaultProps = {
    minRiskLevel: 3,
    maxRiskLevel: 25,
    riskLevel: 3,
};

RiskLevelSelector.propTypes = {
    minRiskLevel: PropTypes.number,
    maxRiskLevel: PropTypes.number,
    riskLevel: PropTypes.number,
    investmentSum: PropTypes.number,
    onChangeRiskLevel: PropTypes.func,
    onChangeInvestmentSum: PropTypes.func
};

export default RiskLevelSelector;
