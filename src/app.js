import React from 'react';
import {BrowserRouter as Router, Route} from "react-router-dom";
import axios from "axios";
import 'regenerator-runtime/runtime'
import Menu from './menu';
import RiskLevelSelector from './risk-level-selector';
import Table from './table';
import Chart from './chart';

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            riskLevel: 3,
            investmentSum: 10000,
            responseData: null
        };
        this.onChangeRiskLevel = this.onChangeRiskLevel.bind(this);
        this.onChangeInvestmentSum = this.onChangeInvestmentSum.bind(this);
    }

    componentDidMount() {
        this.getData();
    }

    onChangeRiskLevel(riskLevel) {
        this.setState({riskLevel});
    }

    onChangeInvestmentSum(investmentSum) {
        this.setState({investmentSum});
    }

    async getData() {
        axios.get('http://localhost:3000/api/cones')
            .then(response => {
                this.setState({
                    responseData:response
                })
            })
            .catch(error => console.error(error));
    }

    render() {
        const {riskLevel, investmentSum, responseData} = this.state;
        return (
            <Router>
                {responseData ?
                    <div>
                        <Menu/>
                        <RiskLevelSelector
                            riskLevel={riskLevel}
                            investmentSum={investmentSum}
                            onChangeRiskLevel={this.onChangeRiskLevel}
                            onChangeInvestmentSum={this.onChangeInvestmentSum}
                        />
                        <Route exact path="/" component={() =>
                            <Table investmentSum={investmentSum}
                                responseData={responseData}
                                riskLevel={riskLevel}/>}
                        />
                        <Route path="/table" component={() =>
                            <Table investmentSum={investmentSum}
                                   responseData={responseData}
                                   riskLevel={riskLevel}/>}
                        />
                        <Route path="/chart" component={() =>
                            <Chart
                                investmentSum={investmentSum}
                                responseData={responseData}
                                riskLevel={riskLevel}/>}
                        />
                    </div>:
                    <div>Loading</div>}
            </Router>
        );
    }

}
