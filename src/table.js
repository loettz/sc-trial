import React from 'react';
import PropTypes from 'prop-types';
import {calculateTimeSeries} from './utils';

class Table extends React.Component {

    render() {
        let {riskLevel, investmentSum, responseData} = this.props;
        const cone = responseData.data.find(cone => cone.riskLevel === riskLevel);
        const fee = 0.01;

        let timeSeries = calculateTimeSeries({
            mu: cone.mu,
            sigma: cone.sigma,
            years: 10,
            initialSum: investmentSum,
            monthlySum: 200,
            fee
        });

        const months = timeSeries.median.map((v, idx) => idx);
        let dataGood = timeSeries.upper95.map(v => v.y);
        let dataMedian = timeSeries.median.map(v => v.y);
        const dataBad = timeSeries.lower05.map(v => v.y);

        const rows = months.map((entry, idx) => (
            <tr key={idx}>
                <td>{entry}</td>
                <td>{dataGood[idx]}</td>
                <td>{dataMedian[idx]}</td>
                <td>{dataBad[idx]}</td>
            </tr>
        ));

        let tableHeader = React.createElement('tr', {}, [
            React.createElement('th', {key: 'month'}, 'month'),
            React.createElement('th', {key: 'good'}, 'good'),
            React.createElement('th', {key: 'median'}, 'median'),
            React.createElement('th', {key: 'bad'}, 'bad')
        ]);

        return (
            <table>
                <thead>
                    {tableHeader}
                </thead>
                <tbody>
                   {rows}
                </tbody>
            </table>
        );
    }

}

Table.propTypes = {
    riskLevel: PropTypes.number,
    investmentSum: PropTypes.number,
    responseData: PropTypes.object
};

export default Table;
